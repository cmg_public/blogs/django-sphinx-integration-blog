# django-notebook

This project is an app built to demonstrate Sphinx documentation integration with Django.

## For building docs:
```
cd into docs directory: cd /docs/
to build the rst files: .\make html
```

Documentation is accessible from ```http://127.0.0.1:8000/docs/```