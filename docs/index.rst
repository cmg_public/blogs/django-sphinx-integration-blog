.. DjangoSphinxBlog documentation master file, created by
   sphinx-quickstart on Wed Jun 28 15:10:00 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DjangoSphinxBlog's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
